This repo aims at documenting settings changes in a gitlab project.

The main entrypoint is the `settings.yaml` file.
This file contains all the current settings that are shared amongst the various projects.

For a full definition of every single option, have a look at the official [documentation](https://docs.gitlab.com/ee/api/projects.html#edit-project).
